﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace AnotherTestGame.Characters
{
    class Player
    {
        #region Attributes
        protected Texture2D texture;
        protected Vector2 position;
        Vector2 velocity;
        public Vector2 Position {
            get { return position; }
            set {
                position = value;
                if (_animationManager != null)
                    _animationManager.Position = position;
            }
        }
        bool hasJumped;
        public Input input;
        protected Dictionary<string, Animation>  _animations;
        protected AnimationManager _animationManager;
        public float Speed = 1.0f;
        #endregion

        public Player(Texture2D texture )
        {
            this.texture = texture;
        }

        public Player(Dictionary<string,Animation> animations)
        {
            this._animations = animations;
            _animationManager = new AnimationManager(_animations.First().Value);
        }

        public void Update(GameTime gameTime)
        {
            KeyboardState ks = Keyboard.GetState();
            Move(ks);
            Jump(ks);
            _animationManager.Update(gameTime);
            SetAnimation();
        }

        protected virtual void SetAnimation()
        {
            if(velocity.X > 0)
            {
                _animationManager.Play(_animations["run"]);
                this._animationManager.ChangeDirection(SpriteEffects.None);
            }

            else if (velocity.X < 0)
            {
                _animationManager.Play(_animations["run"]);
                this._animationManager.ChangeDirection(SpriteEffects.FlipHorizontally);
            }

            else
            {
                _animationManager.Stop();
            }
        }

        protected virtual void Move(KeyboardState ks)
        {
            Position += velocity;

           
            if (ks.IsKeyDown(input.Left))
            {
                this.velocity.X = -Speed;
               
            }

            else if (ks.IsKeyDown(input.Right))
            {
                this.velocity.X = Speed;
                
            }
                

            else this.velocity.X = 0f;
           
        }

        protected virtual void Jump(KeyboardState ks)
        {
            if (ks.IsKeyDown(input.Jump))
            {
                position.Y -= 10f;
                velocity.Y = -5f;
                hasJumped = true;
            }

            if (hasJumped)
            {
                float i = 1;
                velocity.Y += 0.15f * i;
            }

            else
                velocity.Y = 0f;
            //Lógica para alto a la caída(mete colisión aquí)
            if (Position.Y + (_animations.First().Value.FrameHeight) >= 450)
                hasJumped = false;
        }

        public void Draw(SpriteBatch spritebatch)
        {
            if (texture != null)
                spritebatch.Draw(texture, Position, Color.White);

            else if (_animationManager != null)
                _animationManager.Draw(spritebatch);
            else throw new Exception("Something went wrong");
        }

    }
}
