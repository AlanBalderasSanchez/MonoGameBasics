﻿//Alan Balderas Sánchez.
//Based on Oyyu's (Youtube) video series.
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnotherTestGame
{
    //Animador del sprite que utilizamos en Animation
    public class AnimationManager
    {
        private Animation _animation;
        private float _timer;
        public Vector2 Position { get; set; }
        private SpriteEffects _flip;
        public int FrameBegin, FrameEnd;

        public AnimationManager(Animation animation)
        {
            _animation = animation;
            _flip = SpriteEffects.None;
        }

        public void Play(Animation animation)
        {
            //Don't reset the animation, is the one currently being used...so
            if (_animation == animation)
                return;

            //Do change the animation, and set the begining frame and begining col/row 
            FrameBegin = _animation.FrameBegin;
            FrameEnd = _animation.FrameEnd;
            _animation.CurrentFrameRow = rowNumberBegin(FrameBegin, _animation.FrameCountHorizontal);
            _animation.CurrentFrameCol = colNumberBegin(FrameBegin, _animation.CurrentFrameRow, _animation.FrameCountHorizontal);
            _timer = 0;
        }

        public void Stop()
        {
            _timer = 0f;
            _animation.CurrentFrameRow = rowNumberBegin(FrameBegin, _animation.FrameCountHorizontal);
            _animation.CurrentFrameCol = colNumberBegin(FrameBegin,_animation.CurrentFrameRow,_animation.FrameCountHorizontal);
            _animation.CurrentFrame = 0;
        }

        public void Update(GameTime gameTime)
        {
            _timer += (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (_timer > _animation.FrameSpeed)
            {
                _timer = 0;

                //Increase the current frame (just to control the frame we should be watching)
                _animation.CurrentFrame++;
                //Increase current frame column in which we are.
                _animation.CurrentFrameCol++;

                //First we make shure that we haven't search the last frame.
                //If we have, reset the column  and the row in which our frame (based on an index system) is located
                if(_animation.CurrentFrame >= FrameEnd)
                {
                    _animation.CurrentFrameRow = rowNumberBegin(FrameBegin, _animation.FrameCountHorizontal);
                    _animation.CurrentFrameCol = colNumberBegin(FrameBegin, _animation.CurrentFrameRow, _animation.FrameCountHorizontal);
                }

                //If the column has reached the end of the sprite (in case our animation continues in the next row of 
                //sprites, we reset the column count to 0 (begining in the next row) and icnrease the row count.
                if (_animation.CurrentFrameCol >= _animation.FrameCountHorizontal)
                {
                    _animation.CurrentFrameRow++;
                    _animation.CurrentFrameCol = 0;

                }
            }
        }

        //Retrieves the row in which our animation starts (to confirm this algorithm just draw a spritesheet and apply this
        //function.
        private int rowNumberBegin(int frameNumber, int numberOfCols)
        {
            decimal result = frameNumber / numberOfCols;
            return (int) Math.Ceiling(result);
        }

        //Same as above, but to retrieve columns.
        private int colNumberBegin(int frameNumber, int row, int numberOfCols)
        {
            return frameNumber - ((row - 1) * numberOfCols);
        }

        public void ChangeDirection(SpriteEffects flip)
        {
            _flip = flip;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_animation.Texture,
                             Position,
                             new Rectangle(
                                 (_animation.CurrentFrameCol-1) * _animation.FrameWidth,
                                 (_animation.CurrentFrameRow -1) * _animation.FrameHeight,
                                 _animation.FrameWidth,
                                 _animation.FrameHeight
                                 ),Color.White,0f,new Vector2(0,0),1.0f,_flip,0.0f);
        }

    }
}
