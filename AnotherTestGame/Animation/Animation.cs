﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnotherTestGame
{
    //Características del sprite que se utilizará para animar.
    public class Animation
    {
        //Current row in which the frame is located, think of it as a matrix
        public int CurrentFrameRow { get; set; }
        //Current column in which the frame is located, think of it as a matrix
        public int CurrentFrameCol { get; set; }
        //Actual frame that the player is watching, based on index system.
        public int CurrentFrame { get; set; }
        //Número de frames que contiene una sola animación a lo largo
        public int FrameCountHorizontal { get; set; }
        //Número de frames que contiene una sola animación a lo ancho
        public int FrameCountVertical { get; set; }
        //Altura de ún frame en el spritesheet
        public int FrameHeight { get { return Texture.Height/FrameCountVertical;  } }
        //Anchura de ún frame en el spritesheet
        public int FrameWidth { get { return Texture.Width / FrameCountHorizontal; } }
        //Velocidad con la que vamos recorriendo cada frame
        public float FrameSpeed { get; set; }
        //Si la animación se ejecuta indeterminadamente
        public bool isLoopig { get; set; }

        public Texture2D Texture { get; private set; }

        public int FrameBegin, FrameEnd;

        public Animation(Texture2D texture, int frameCountHorizontal,int frameCountVertical,int frameBegin, int frameEnd)
        {
            Texture = texture;
            FrameCountHorizontal = frameCountHorizontal;
            FrameCountVertical = frameCountVertical;
            FrameBegin = frameBegin;
            FrameEnd = frameEnd;
            //Default
            isLoopig = true;
            FrameSpeed = 0.2f;
        }   
    }
}
